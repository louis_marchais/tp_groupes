using System;
namespace TP_groupes
{
    class TestMatrixTab
    {
        static void Main(string[] args)//méthode d'appelle principale de notre programme
        {
                int [, ] test = { {1,2 }, {9, 7}};//valeurs de la matrice
                MatrixTab sq = new MatrixTab(test);//instancies des valeurs dans la matrice
                Console.WriteLine(sq.ToString());//affiche la matrice
                Console.WriteLine(sq.GetValue(0, 1));//va chercher la valeur a la case x:1 et y:0
                Console.WriteLine(sq.ChangeValue(0, 1, 6));//modifie la valeurs de case x:1 et y:0 par 6
                Console.WriteLine(sq.GetValue(0, 1));
                Console.WriteLine(sq.ToString());//affiche la matrice
        } 
    }
}    
