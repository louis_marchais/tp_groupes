﻿using System;

namespace TP_groupes
{
    class MatrixTab
    {
        private int [,] sqmatrix; //définition des propriétés
        private int size;

        public MatrixTab(int[,] sqmatrix)// créer une matrice
        {
            this.sqmatrix = sqmatrix;
            //this.size = sqmatrix.Length;
            this.size = sqmatrix.GetLength(0);
        }

        public MatrixTab(int size, int defaultValue)// constructeur qui ajoute les valeurs dans la matrice
        {
            this.sqmatrix = new int [size, size];
            this.size = size;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    this.sqmatrix[i, j] = defaultValue;
                }
            }
        }
        public string ToString()//parcours la matrice, la concatène dans une string, et la retroune
        {
            string ret = "";
            for (int i = 0; i < this.size; i++)
            {
                //Console.Write("| ");
                ret = ret + "| ";
                for (int j = 0; j < this.size; j++)
                {
                    //Console.Write(test[i,j] + " | ");
                    ret = ret + this.sqmatrix[i, j] + " | ";
                }
                //Console.WriteLine();
                ret = ret + "\n";
            }
            return ret;
        }

        public int GetValue(int y, int x)//va chercher la valeur dans le tableau (matrix)
        {
            return this.sqmatrix[x, y];
        }

        public string ChangeValue(int y, int x, int new_value) //change une valeur du tableau
        {
            this.sqmatrix[x, y] = new_value;
            return "Change value DONE";
        }
    }
}